# If you come from bash you might have to change your $PATH.
# export PATH=$HOME/bin:/usr/local/bin:$PATH

# Path to your oh-my-zsh installation.
export ZSH="/Users/diegoazd/.oh-my-zsh"
export PATH=$PATH:$HOME/.linkerd2/bin
export PATH=~/istio-1.1.0/bin:$PATH

# Set name of the theme to load --- if set to "random", it will
# load a random theme each time oh-my-zsh is loaded, in which case,
# to know which specific one was loaded, run: echo $RANDOM_THEME
# See https://github.com/robbyrussell/oh-my-zsh/wiki/Themes
ZSH_THEME="cdimascio-lambda"

source $ZSH/oh-my-zsh.sh

# User configuration

# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# ssh
# export SSH_KEY_PATH="~/.ssh/rsa_id"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"
source /usr/local/share/zsh-autosuggestions/zsh-autosuggestions.zsh
ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE='fg=3'
plugins=(git colored-man colorize pip python brew osx zsh-syntax-highlighting java brew docker git-flow osx npm node pip sbt scala tmux vi-mode httpie gradle)
export PATH=/Applications/Visual\ Studio\ Code.app/Contents/Resources/app/bin:$PATH
export GOOGLE_APPLICATION_CREDENTIALS=~/Documents/keys/kubo-financiero-portal-d54b3db61ce7.json

#THIS MUST BE AT THE END OF THE FILE FOR SDKMAN TO WORK!!!
export SDKMAN_DIR="/Users/diegoazd/.sdkman"
[[ -s "/Users/diegoazd/.sdkman/bin/sdkman-init.sh" ]] && source "/Users/diegoazd/.sdkman/bin/sdkman-init.sh"

 export NVM_DIR="$HOME/.nvm"
   . "/usr/local/opt/nvm/nvm.sh"

   gpip(){
     PIP_REQUIRE_VIRTUALENV="0" pip3 "$@"
   }


# The next line updates PATH for the Google Cloud SDK.
if [ -f '/Users/diegoazd/Downloads/google-cloud-sdk/path.zsh.inc' ]; then . '/Users/diegoazd/Downloads/google-cloud-sdk/path.zsh.inc'; fi

# The next line enables shell command completion for gcloud.
if [ -f '/Users/diegoazd/Downloads/google-cloud-sdk/completion.zsh.inc' ]; then . '/Users/diegoazd/Downloads/google-cloud-sdk/completion.zsh.inc'; fi
export PATH="/usr/local/opt/gettext/bin:$PATH"
if [ /usr/local/bin/kubectl ]; then source <(kubectl completion zsh); fi
